'''
Created on 24.11.2019

@author: Heinz Schenk
'''
#pylint: disable=missing-class-docstring,missing-function-docstring,invalid-name

import unittest
from converter import convert_function


class Test_converter_functions(unittest.TestCase):

    def test_bare_public(self):
        original = 'Public Function test()'
        expected = 'def test():\n'
        actual = convert_function(original)
        self.assertEqual(actual, expected)

    def test_withtype_public(self):
        original = 'Public Function test() As Integer'
        expected = 'def test():\n'
        actual = convert_function(original)
        self.assertEqual(actual, expected)

    def test_withparams1_public(self):
        original = 'Public Function test(one As Integer) As Integer'
        expected = 'def test(one):\n'
        actual = convert_function(original)
        self.assertEqual(actual, expected)

    def test_withparams1_public_byval(self):
        original = 'Public Function test(ByVal one As Integer) As Integer'
        expected = 'def test(one):\n'
        actual = convert_function(original)
        self.assertEqual(actual, expected)

    def test_withparams1_public_byref(self):
        original = 'Public Function test(ByRef one As Integer) As Integer'
        expected = 'def test(one):\n'
        actual = convert_function(original)
        self.assertEqual(actual, expected)

    def test_withparams2_public(self):
        original = 'Public Function test(one As Integer, two As String) As Integer'
        expected = 'def test(one, two):\n'
        actual = convert_function(original)
        self.assertEqual(actual, expected)

    def test_bare_private(self):
        original = 'Private Function test()'
        expected = 'def test():\n'
        actual = convert_function(original)
        self.assertEqual(actual, expected)

    def test_withtype_private(self):
        original = 'Private Function test() As Integer'
        expected = 'def test():\n'
        actual = convert_function(original)
        self.assertEqual(actual, expected)

    def test_withparams1_private(self):
        original = 'Private Function test(one As Integer) As Integer'
        expected = 'def test(one):\n'
        actual = convert_function(original)
        self.assertEqual(actual, expected)

    def test_withparams2_private(self):
        original = 'Private Function test(one As Integer, two As String) As Integer'
        expected = 'def test(one, two):\n'
        actual = convert_function(original)
        self.assertEqual(actual, expected)

    def test_bare_none(self):
        original = 'Function test()'
        expected = 'def test():\n'
        actual = convert_function(original)
        self.assertEqual(actual, expected)

    def test_withtype_none(self):
        original = 'Function test() As Integer'
        expected = 'def test():\n'
        actual = convert_function(original)
        self.assertEqual(actual, expected)

    def test_withparams1_none(self):
        original = 'Function test(one As Integer) As Integer'
        expected = 'def test(one):\n'
        actual = convert_function(original)
        self.assertEqual(actual, expected)

    def test_withparams2_none(self):
        original = 'Function test(one As Integer, two As String) As Integer'
        expected = 'def test(one, two):\n'
        actual = convert_function(original)
        self.assertEqual(actual, expected)


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
