# pylint: disable=line-too-long

'''
Public declarations used project-wide

.. data:: int EXIT_OK = 0

    Exit code for "everything is ok".
    This exit code is set if no error occurred during the tests.

.. data:: int EXIT_ERROR = 1

    Exit code for "something went wrong".
    This exit code is set if there has been any error during the tests detected.

.. data:: bool verbose

    Mirrors the ``-v/--verbose`` command line option for public access

.. data:: bool debug

    Mirrors the ``--debug`` command line option for public access
'''

# Command line exit codes
EXIT_OK = 0
EXIT_ERROR = 1

verbose = False  # pylint: disable=invalid-name
debug = False  # pylint: disable=invalid-name
