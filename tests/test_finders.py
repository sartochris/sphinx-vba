'''
Created on 09.12.2019

:author: Christoph.Juengling
'''
import unittest

from converter import find_docstrings, find_functions, convert_module


#pylint: disable=missing-function-docstring, invalid-name, missing-class-docstring
class Test_finders(unittest.TestCase):

    _code = """'''
' Write a line of text into the log file
'
' :param text: Text
' :author: Christoph Juengling
'''
Public Sub WriteLine(Optional ByVal text As String = "")

Const FUNCTION_NAME = "WriteLine"

End Sub

Public Function filename(ByVal strFilename As String)

Const FUNCTION_NAME = "filename"

End Function
""".split('\n')

    def test_finddocstrings(self):
        docstrings = find_docstrings(self._code)
        self.assertTrue(isinstance(docstrings, dict))
        self.assertEqual(len(docstrings), 1)

    def test_finddocstrings_check_types(self):
        docstrings = find_docstrings(self._code)
        for doc in docstrings:
            self.assertTrue(isinstance(doc, tuple), 'Not a tuple')
            self.assertEqual(len(doc), 2, 'Tuple doesn\'t contain 2 items')
            start, end = doc
            self.assertGreater(end, start, 'End line is not greater than start line')

    def test_findfunctions(self):
        functions = find_functions(self._code)
        self.assertTrue(isinstance(functions, dict))
        self.assertEqual(len(functions), 2)

    def test_convertmodule(self):
        expected = """def WriteLine(text):
    '''
    Write a line of text into the log file

    :param text: Text
    :author: Christoph Juengling
    '''

def filename(strFilename):
"""
        actual = convert_module(self._code)
        self.assertEqual(actual, expected)

    def test_convertmodule_doc(self):
        code = """'''
Module description
'''

'''
Function description
'''
Public Sub myfunc(hello)

End Sub
""".split('\n')

        expected = """'''
Module description
'''

def myfunc(hello):
    '''
    Function description
    '''

"""
        actual = convert_module(code)
        self.assertEqual(actual, expected)


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
